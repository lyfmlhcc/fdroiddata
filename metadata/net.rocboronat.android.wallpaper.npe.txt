Categories:Wallpaper,Multimedia
License:GPLv3
Web Site:https://code.google.com/p/black-npe-live-wallpaper/
Source Code:https://code.google.com/p/black-npe-live-wallpaper/source/browse
Issue Tracker:https://code.google.com/p/black-npe-live-wallpaper/issues

Auto Name:Null Black Wallpaper
Summary:Live wallpaper that does nothing
Description:
Use a blank pattern as "live wallpaper".
.

Repo Type:hg
Repo:https://code.google.com/p/black-npe-live-wallpaper/

Build:3.14,3
    commit=fa06e030ad36

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:3.14
Current Version Code:3

