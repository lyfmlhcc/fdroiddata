Categories:Office
License:GPLv2+
Web Site:http://www.mitzuli.com
Source Code:https://github.com/artetxem/mitzuli
Issue Tracker:https://github.com/artetxem/mitzuli/issues

Auto Name:Mitzuli
Summary:Offline Translator
Description:
Translator featuring a full offline mode, voice input (ASR), camera input
(OCR), voice output (TTS), and more!

This app periodically updates the list of available languages and their
respective resources in the background. This is necessary for the app to
work robustly, and no personal information is sent to the server.
.

Repo Type:git
Repo:https://github.com/artetxem/mitzuli

Build:1.0.2,10002
    commit=20f2c2f36e593f8c97fadecb3b95ed6cc0bad759
    subdir=app
    gradle=yes
    prebuild=sed -i -e '29,37d' build.gradle && sed -i -e '/splits/,+7d' build.gradle && echo 'ndkdir=$$NDK$$' > ../gradle.properties
    scanignore=app/src/main/java/com/mitzuli/core/mt/MtPackage.java
    ndk=r10d

Maintainer Notes:
We should support gradle abi splits, e.g. by re-using the output= field to
define which output apk we should use.
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.2
Current Version Code:10002

