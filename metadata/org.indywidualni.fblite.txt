AntiFeatures:NonFreeNet
Categories:Internet
License:GPLv2+
Web Site:https://github.com/indywidualny/FacebookLite/blob/HEAD/README.md
Source Code:https://github.com/indywidualny/FacebookLite
Issue Tracker:https://github.com/indywidualny/FacebookLite/issues

Auto Name:Facebook Lite
Summary:Connect to Facebook
Description:
Inofficial app build around the mobile Facebook site.

[https://github.com/indywidualny/FacebookLite/releases Changelog]
.

Repo Type:git
Repo:https://github.com/indywidualny/FacebookLite

Build:1.3.2,1
    commit=v1.3.2
    subdir=app
    gradle=yes

Build:1.4.0,2
    commit=v1.4.0
    subdir=app
    gradle=yes

Build:1.4.1,3
    commit=v1.4.1
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.4.1
Current Version Code:3

